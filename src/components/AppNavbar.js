
import {useState} from 'react' //-> import the  hook 
import {Link} from 'react-router-dom';
import{Navbar, Container, Nav} from 'react-bootstrap' /// --> import object in comes to get some element from react-bootstrap



export default function  AppNavbar() {    

  // state hook to store the user information strored in the login page
  const [user, setUser] = useState(localStorage.getItem('email'))
  console.log(user)

    
        return (
        

            <Navbar bg="light" expand="lg">
            <Container>
                  <Navbar.Brand as={Link} to ="/">Batch-196 Booking App</Navbar.Brand>
                  <Navbar.Toggle aria-controls="basic-navbar-nav" />
                  <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                  
                        <Nav.Link as ={Link} to="/">Home</Nav.Link>
                        <Nav.Link as={Link} to ="/Course">Coures</Nav.Link>
                   {     
                      (user !== null)?
                           <Nav.Link as={Link} to = "/logout">Logout</Nav.Link>
                      :
                      <>
                        <Nav.Link as={Link} to ="/Login">Login</Nav.Link>
                        <Nav.Link as={Link} to ="/Rigister">Register</Nav.Link>

                        </>
                   }
                        
                     
                    </Nav>
                  </Navbar.Collapse>
            </Container>
          </Navbar>
        )
};