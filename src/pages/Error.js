import {Row, Col, Button} from 'react-bootstrap'

import {Link} from 'react-router-dom';

export default function Error() {
    

    return(

             <Row>
                <Col claasName = "p-5">
                    <h1>404 - Page Not found</h1>
                    <p>The page you are looking for cannot be found or has been noved</p>
                
                    <Button variant="primary" as={Link} to ="/">Back Home</Button>

                </Col>
        </Row>
    
    )


}