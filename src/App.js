
import {Container} from 'react-bootstrap'

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'; //---> uses for router in every page component that rendering 
import AppNavbar from './components/AppNavbar'; // ->  -> import from components folder and determine the file specific
// import Banner from './components/Banner'; // ->
// import CourseCard  from "./components/CourseCard";
// import Highlights from './components/Highlights';
import Course from './pages/Courses';
import Home from './pages/Home';
import Rigister from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import './App.css';

// /* <></> */ --> reat fragment
function App() {
  return (
    <> 
    {/* to allowed to to rourter */}
    <Router>
            <AppNavbar/>
              <Container>
                {/* routes is the specific in pages */}
                  {/* Route is the most specific in every page to rendering*/}
                  <Routes> 
                        <Route exact path="/" element={<Home/>}/>
                        <Route exact path="/Course" element={<Course/>}/>
                        <Route exact path="/Rigister" element={<Rigister/>}/>
                        <Route exact path="/Login" element={<Login/>}/>
                        <Route exact path="/Logout" element={<Logout/>}/>
                        <Route exact path="/Error" element={<Error/>}/>
                   </Routes>
                        
              </Container>        
      </Router>
    </>
  )
}

export default App;
